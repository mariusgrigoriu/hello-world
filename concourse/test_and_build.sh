#!/bin/bash

set -e -x

export GOPATH=$(pwd)/go
root=$(pwd)
pushd go/src/git.nordstrom.net/k8s/hello-world
  make test_app
  make build_app
  mv build/* $root/build
popd
