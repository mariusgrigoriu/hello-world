FROM quay.io/nordstrom/baseimage-ubuntu:16.04
MAINTAINER Kubernetes Platform Team "invcldtm@nordstrom.com"

ADD hello-world /hello-world
COPY images /images

ENTRYPOINT /hello-world
