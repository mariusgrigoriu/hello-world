package main

var Version string = ""

type VersionInfo struct {
}

func (v VersionInfo) GetVersion() string {
	if Version == "" {
		Version = "1.0"
	}

	return Version
}
