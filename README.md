# Hello World Example Golang Service

This service is an example to highlight how to do a golang service in the platform.

Some platform features of note visible in this repo:

 * templating of Kubernetes' API object YAML files using Sigil
   * separation of embedded templates: alert rule annotations are now external

This document needs to be revised.
