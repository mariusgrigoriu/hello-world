project_root := $(realpath $(PWD))
build_root := $(project_root)/build
git_sha_commit := $(shell git rev-parse --short=8 HEAD)


# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

export app_name := hello-world
export namespace := default
export image_tag := $(git_sha_commit)
export SIGIL := SIGIL_DELIMS={{{,}}} bin/sigil

.PHONY: build_app test_app
.PHONY: teardown clean

# Return filenames (basename) of all files in templates directory with .tmpl.yaml extension
TEMPLATES := $(shell find $(project_root)/templates/ -name '*.yaml' -exec basename {} \;)
# Returns the TEMPLATES list substituting %.yaml.tmpl with %.yaml and prepending $(build_root)/ to it
OBJECTS :=  $(addprefix $(build_root)/k8s/,$(TEMPLATES))
# Returns OBJECTS list substituting %.yaml with %.deployed
DEPLOYCHECKS := $(OBJECTS:%.yaml=%.deployed)

build_app: | $(build_root) $(OBJECTS)
	GOOS=linux GOARCH=amd64 go build -o $(build_root)/$(app_name) -ldflags "-X main.Version=$(image_tag)"
	cp Dockerfile $(build_root)
	echo $(git_sha_commit) > $(build_root)/tag
	cp -r images $(build_root)

templates: $(build_root) $(OBJECTS)

test_app:
	go test

local_build:
	docker run golang:1.7.1 make test build

teardown:
	kubectl delete -l app=$(app_name) --namespace=$(namespace) deployment
	kubectl delete -l app=$(app_name) --namespace=$(namespace) ingress
	kubectl delete -l app=$(app_name) --namespace=$(namespace) service
	kubectl delete -l app=$(app_name) --namespace=$(namespace) configmap

clean:
	rm -rf $(build_root)

$(build_root)/k8s/%.yaml: $(project_root)/templates/%.yaml | $(build_root)
	@echo "Templating $@"
	$(SIGIL) -p -f "$<" > "$@"

$(build_root):
	mkdir -p "$@/k8s"
