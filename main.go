package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
  "os"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	fooCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "fooCounter",
		Help: "Number of times /foo endpoint is called.",
	})
	logCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "logCounter",
		Help: "Number of times /log endpoint is called.",
	})
)

var corsHeaders = map[string]string{
	"Access-Control-Allow-Headers":  "Accept, Authorization, Content-Type, Origin",
	"Access-Control-Allow-Methods":  "GET, OPTIONS",
	"Access-Control-Allow-Origin":   "*",
	"Access-Control-Expose-Headers": "Date",
}

func init() {
	prometheus.MustRegister(fooCounter)
	prometheus.MustRegister(logCounter)
}

func main() {
	flags := parseFlags()
	versionInfo := &VersionInfo{}

	if flags.version {
		fmt.Println(versionInfo.GetVersion())
		return
	}

	address := fmt.Sprintf(":%d", flags.port)
	log.Fatal(http.ListenAndServe(address, getMux(versionInfo)))
}

func getMux(versionInfo *VersionInfo) *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/foo", fooHandler)
	mux.HandleFunc("/log", logHandler)
	mux.HandleFunc("/health", healthHandler)
	mux.Handle("/metrics", prometheus.Handler())
	mux.HandleFunc("/", mainHandler)
	mux.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("images"))))
	return mux
}

// Enables cross-site script calls.
func setCORS(w http.ResponseWriter) {
	for h, v := range corsHeaders {
		w.Header().Set(h, v)
	}
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	setCORS(w)
	w.WriteHeader(http.StatusOK)
}

func fooHandler(w http.ResponseWriter, r *http.Request) {
	message := fmt.Sprintf("/foo called at %s\n", time.Now())
	fmt.Printf(message)

	fooCounter.Inc()
	setCORS(w)
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, message)
}

func logHandler(w http.ResponseWriter, r *http.Request) {
  hostname, _ := os.Hostname()
	message := fmt.Sprintf("/log called at %s\nHostname: %s", time.Now(), hostname)
	fmt.Printf(message)

	logCounter.Inc()
	setCORS(w)
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, message)
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("/ called at %s\n", time.Now())
	setCORS(w)
	w.WriteHeader(http.StatusOK)

	output :=
		`<html><head><style>html {
	  background: url(images/logos.png) no-repeat center center fixed;
	  -webkit-background-size: cover;
	  -moz-background-size: cover;
	  -o-background-size: cover;
	  background-size: cover;
		}</style></head><body /></html>`
	io.WriteString(w, output)
}

type Flags struct {
	port    int
	version bool
}

func parseFlags() (flags Flags) {
	flag.IntVar(&flags.port, "port", 8080, "port on which the service will be listening")
	flag.BoolVar(&flags.version, "version", false, "Return version.")
	flag.Parse()
	return
}
